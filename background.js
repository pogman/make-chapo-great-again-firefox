browser.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  const facebookUrlPattern = /.+?(?=:\/\/):\/\/www\.facebook\.com\/.*/gm;

  if (changeInfo.status == "complete" && facebookUrlPattern.test(tab.url)) {
    browser.tabs.executeScript(tab.id, { file: "content.js" }); 
  }
});

