const toArray = (collection) => {
  return Array.prototype.slice.call(collection);
}

// allImagesOnPage :: () -> [HTMLImageElement]
const allImagesOnPage = () => {
  return toArray(document.getElementsByTagName('img'));
};

// isChapo :: HTMLImageElement -> Bool
const isChapo = (image) => {
  const chapoUrlPattern = /https:\/\/static\.xx\.fbcdn\.net\/images\/emoji\.php\/v9\/.+?(?=\/1f438\.png)\/1f438\.png/gm;
  return chapoUrlPattern.test(image.getAttribute("src"));
};

// filterChapos :: [HTMLImageElement] -> [HTMLImageElement]
const filterChaposFrom = (images) => {
  return images.filter(isChapo);
};

const oldChapo = "https://emojipedia-us.s3.amazonaws.com/thumbs/120/facebook/65/frog-face_1f438.png"

const applyDomChanges = () => {
  filterChaposFrom(allImagesOnPage()).forEach(chapo => {
    chapo.src = oldChapo;
  });
};

setInterval(applyDomChanges, 10);
